import { Route, Routes } from "react-router-dom";

import Service from "../page/Service";
import Home from "../page/user/Home";

export default function User() {
  return (
    <Routes>
      <Route path="/" element={<Home />} />

      <Route path="/service" element={<Service />} />
    </Routes>
  );
}
