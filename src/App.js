import { useLocation } from 'react-router-dom';
import DefaultLayoutUser from "../src/component/defaultLayoutUser";

function App() {
  const location = useLocation();
  const currentPath = location.pathname;

  return <DefaultLayoutUser />
}

export default App;
