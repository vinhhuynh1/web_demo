
// import AnimationDot from "@/app/components/common/AnimationDot";
// import Line from "@/components/common/Line";
import { useDevice } from "../../hook/useDevice";

import { Col, Row, message } from "antd/lib";
import { React, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axiosInstance from "../../until/axios";
import { Card } from "antd";
import styles from "./index.module.css";
import table from "../../component/Image/images/table.png";
import arrows from "../../component/Image/images/arrows.svg";
const Service = () => {
  const { isMobile } = useDevice();

  const dataPakage = [
    {
      name: "BASIC",
      price: "899$",
      title1: "10 design pages",
      title2: "Well-documented",
      title3: "4 revisions",
      title4: "$100/additional page",
    },
    {
      name: "PRO",
      price: "1299$",

      title1: "10 design pages",
      title2: "Well-documented",
      title3: "4 revisions",
      title4: "$100/additional page",
    },
    {
      name: "VIP",
      price: "1999$",
      title1: "10 design pages",
      title2: "Well-documented",
      title3: "4 revisions",
      title4: "$100/additional page",
    },
  ];

  const [isExpanded, setIsExpanded] = useState(false);

  const handleArrowClick = () => {
    setIsExpanded(!isExpanded);
  };

  const [isExpanded1, setIsExpanded1] = useState(false);

  const handleArrowClick1 = () => {
    setIsExpanded1(!isExpanded1);
  };

  const [isExpanded2, setIsExpanded2] = useState(false);

  const handleArrowClick2 = () => {
    setIsExpanded2(!isExpanded2);
  };

  const [isExpanded3, setIsExpanded3] = useState(false);

  const handleArrowClick3 = () => {
    setIsExpanded3(!isExpanded3);
  };

  const [isExpanded4, setIsExpanded4] = useState(false);

  const handleArrowClick4 = () => {
    setIsExpanded4(!isExpanded4);
  };

  return (
    <div className=" pb-28">
      <div className="pl-[10%] pr-[10%] pt-20 ">
        <div className="text-[#42495B] text-center font-bold text-2xl mb-5">
          <div className="text-[#16FCD2] text-[16px] font-[700] font-[Noto Sans] leading-[48px]">
            ABOUT METAFORMA
          </div>
          <div className="flex flex-col items-center">
            <div className="text-white text-[30px] font-[700] font-[Noto Sans] leading-[48px]">
              La plateforme tout en un pour les professionnels de la
            </div>
            <div className="text-white text-[30px] font-[700] font-[Noto Sans] leading-[48px]">
              formation la plus simple et automatisée au monde
            </div>
          </div>
        </div>
        <div className="pt-9 pb-9">
          <div className="h-[1px] bg-[#FFFFFF]"></div>
        </div>
        {/* --------------- */}

        <div className="flex justify-center pb-10">
          {dataPakage.map((data, index) => {
            return (
              <div key={index}>
                <div className="pr-9">
                  <Card className={`${styles.bg_transparent}`}>
                    <div className="flex items-center pl-5 pr-5">
                      <div className="text-[#16FCD2] text-[30px] font-[700] font-[Noto Sans] leading-[48px] pr-6">
                        {data.name}
                      </div>
                      <div>
                        <div className="text-[#928E91] text-[12px] font-[400] font-[Noto Sans] leading-[48px]">
                          Starting from
                        </div>
                        <div className="text-white text-[36px] font-[700] font-[Noto Sans] leading-[48px]">
                          {data.price}
                        </div>
                      </div>
                    </div>
                    <div className="pt-9 pb-9">
                      <div className="h-[0.2px] bg-[#6f6d6d]"></div>
                      <div className="flex flex-col items-center pt-5 pb-5">
                        <div className="text-white text-[16px] font-[500] font-[Noto Sans] leading-[37.5px]">
                          {data.title1}
                        </div>
                        <div className="text-white text-[16px] font-[500] font-[Noto Sans] leading-[37.5px]">
                          {data.title2}
                        </div>
                        <div className="text-white text-[16px] font-[500] font-[Noto Sans] leading-[37.5px]">
                          {data.title3}
                        </div>
                        <div className="text-white text-[16px] font-[500] font-[Noto Sans] leading-[37.5px]">
                          {data.title4}
                        </div>
                      </div>
                    </div>
                    <button className="bg-[#DD5313] text-white pr-20 pl-20 pt-4 pb-4 text-[16px] rounded leading-[22px] font-[600] hover:bg-white hover:text-[#197C6A]">
                      Choose Plan
                    </button>
                  </Card>
                </div>
              </div>
            );
          })}
        </div>
        {/* ------------- */}
        <div className="pb-16">
          <div className="text-[#42495B] text-center font-bold text-2xl mb-5 pt-10">
            <div className="text-[#16FCD2] text-[16px] font-[700] font-[Noto Sans] leading-[48px]">
              FAQ
            </div>
            <div className="flex flex-col items-center">
              <div className="text-white text-[30px] font-[700] font-[Noto Sans] leading-[48px]">
                Frequently asked questions,
              </div>
              <div className="text-white text-[30px] font-[700] font-[Noto Sans] leading-[48px]">
                maybe the same as yours
              </div>
            </div>
          </div>
          <div className="flex justify-center">
            <div className="pr-5">
              <img
                src={table}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>
            <div className="w-[500px]">
              <div className="flex flex-col pb-8">
                <div className="flex flex-row items-center">
                  {" "}
                  <div className="text-white text-[20px] font-[700] font-[Noto Sans] leading-[48px]">
                    How is the payment system?
                  </div>
                  <div className="w-[24px] h-[24px] mt-2">
                    <img
                      src={arrows}
                      alt="Fail"
                      layout="fill"
                      className={`w-[24px] h-[24px] transform ${
                        isExpanded ? "rotate-180" : ""
                      }`}
                      onClick={handleArrowClick}
                    />
                  </div>
                </div>
                {isExpanded && (
                  <div className="text-[#9D9DA7] text-[16px] font-[400] leading-[24px] mt-3">
                    The payment system is designed to be secure and convenient.
                    We offer various payment methods to cater to your needs. If
                    you have any specific questions about the payment process,
                    feel free to reach out to our support team.
                  </div>
                )}
              </div>
              {/* ----------- */}

              <div className="flex flex-col pb-8 ">
                <div className="flex flex-row items-center">
                  {" "}
                  <div className="text-white text-[20px] font-[700] font-[Noto Sans] leading-[48px]">
                    Can I consult first?
                  </div>
                  <div className="w-[24px] h-[24px] mt-2">
                    <img
                      src={arrows}
                      alt="Fail"
                      layout="fill"
                      className={`w-[24px] h-[24px] transform ${
                        isExpanded1 ? "rotate-180" : ""
                      }`}
                      onClick={handleArrowClick1}
                    />
                  </div>
                </div>
                {isExpanded1 && (
                  <div className="text-[#9D9DA7] text-[16px] font-[400] leading-[24px] mt-3">
                    The payment system is designed to be secure and convenient.
                  </div>
                )}
              </div>
              {/* ----------- */}

              <div className="flex flex-col pb-8 ">
                <div className="flex flex-row items-center">
                  {" "}
                  <div className="text-white text-[20px] font-[700] font-[Noto Sans] leading-[48px]">
                    What if the project stops halfway?
                  </div>
                  <div className="w-[24px] h-[24px] mt-2">
                    <img
                      src={arrows}
                      alt="Fail"
                      layout="fill"
                      className={`w-[24px] h-[24px] transform ${
                        isExpanded2 ? "rotate-180" : ""
                      }`}
                      onClick={handleArrowClick2}
                    />
                  </div>
                </div>
                {isExpanded2 && (
                  <div className="text-[#9D9DA7] text-[16px] font-[400] leading-[24px] mt-3">
                    The payment system is designed to be secure and convenient.
                  </div>
                )}
              </div>

              {/* ----------- */}

              <div className="flex flex-col pb-8">
                <div className="flex flex-row items-center">
                  {" "}
                  <div className="text-white text-[20px] font-[700] font-[Noto Sans] leading-[48px]">
                    Does it include servers and domains?
                  </div>
                  <div className="w-[24px] h-[24px] mt-2">
                    <img
                      src={arrows}
                      alt="Fail"
                      layout="fill"
                      c
                      className={`w-[24px] h-[24px] transform ${
                        isExpanded3 ? "rotate-180" : ""
                      }`}
                      onClick={handleArrowClick3}
                    />
                  </div>
                </div>
                {isExpanded3 && (
                  <div className="text-[#9D9DA7] text-[16px] font-[400] leading-[24px] mt-3">
                    The payment system is designed to be secure and convenient.
                  </div>
                )}
              </div>

              {/* ----------- */}

              <div className="flex flex-col pb-8">
                <div className="flex flex-row items-center">
                  {" "}
                  <div className="text-white text-[20px] font-[700] font-[Noto Sans] leading-[48px]">
                    How is the payment system?
                  </div>
                  <div className="w-[24px] h-[24px] mt-2">
                    <img
                      src={arrows}
                      alt="Fail"
                      layout="fill"
                      className={`w-[24px] h-[24px] transform ${
                        isExpanded4 ? "rotate-180" : ""
                      }`}
                      onClick={handleArrowClick4}
                    />
                  </div>
                </div>
                {isExpanded4 && (
                  <div className="text-[#9D9DA7] text-[16px] font-[400] leading-[24px] mt-3">
                    The payment system is designed to be secure and convenient.
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>

        {/* ................... */}

        <div className="h-[0.2px] bg-[#6f6d6d]"></div>

        <div className="flex justify-center items-center pt-12 pb-12">
          <div className="pr-14">
            <div className="text-white text-[30px] font-[700] font-[Noto Sans] leading-[48px]">
            Lorem Ipsum has been the industry's
            </div>
            <div className="text-white text-[30px] font-[700] font-[Noto Sans] leading-[48px]">
            standard dummy text ever 
            </div>
          </div>
          <div className="flex">
            <div className="pr-3">
              <button className="bg-[#6017FD] text-white pr-20 pl-20 pt-6 pb-6 text-[16px] rounded leading-[22px] font-[600] hover:bg-white hover:text-[#197C6A]">
                Demo
              </button>
            </div>
            <div className="">
              <button className="bg-stone-500 text-white pr-20 pl-20 pt-6 pb-6 text-[16px] rounded leading-[22px] font-[600] hover:bg-white hover:text-[#197C6A]">
                Contact
              </button>
            </div>
          </div>
        </div>

        <div className="h-[0.2px] bg-[#6f6d6d]"></div>
      </div>
    </div>
  );
};

export default Service;
