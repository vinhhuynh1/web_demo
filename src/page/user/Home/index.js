import Banner from "../../../component/ui/Banner/index";
import WhatMakeUsDifferent from "../../../component/ui/WhatMakeUsDifferent/WhatMakeUsDifferent";
import About from "../../../component/ui/AboutUs/index";
import Advantages from "../../../component/ui/Advantages/index";
import People from "../../../component/ui/People";
export default function Home() {
  return (
    <>
      <Banner />
      <About />
      <People />
      <Advantages />
      <WhatMakeUsDifferent />
    </>
  );
}
