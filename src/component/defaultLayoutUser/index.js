import User from "../../route/user.route";
import Footer from "../footer";
import Header from "../header";
import { StyleProvider } from '@ant-design/cssinjs';
import background from "../Image/images/bg.png";

export default function DefaultLayoutUser() {
  return (
    <StyleProvider hashPriority="high">
      <div style={{ backgroundImage: `url(${background})` }}> {/* Set the background image */}
        <Header />
        <User />
        <Footer />
      </div>
    </StyleProvider>
  );
}