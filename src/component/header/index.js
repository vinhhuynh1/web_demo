import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { useDevice } from "../../hook/useDevice";
import { Link, useNavigate } from "react-router-dom";

import Arrow from "../Image/images/button.svg";
import Menu from "../Image/images/menuBar.svg";
import Logo from "../Image/images/LogoOgma.svg";
import IconX from "../Image/images/iconX.svg";
const routes = [
  {
    name: "Fonctionnalités",
    pathNames: ["/"],
    path: "/",
  },
  {
    name: "Qualiopi",
    pathNames: ["/service", "/dich-vu"],
    path: "service",
  },
  {
    name: "Tarifs",
    pathNames: [""],
    path: "",
  },
  {
    name: "Blog",
    pathNames: [""],
    path: "",
  },
  {
    name: "Cas client",
    pathNames: [""],
    path: "",
  },
  {
    name: "Démo en direct",
    pathNames: [""],
    path: "",
  },
  {
    name: "Commencer gratuitement",
    pathNames: [""],
    path: "",
  },
  // {
  //   name: "Đăng nhập",
  //   pathNames: ["/careers", "/tuyen-dung"],
  //   path: "/loginAdmin",
  // },
  {
    name: "",
    pathNames: [""],
    path: "/pages/bookProject",
  },
];

export default function Header() {
  const navigate = useNavigate();
  const { t } = useTranslation("translation");
  const { isMobile, isTablet } = useDevice();
  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false);
  const [menuIcon, setMenuIcon] = useState(Menu);
  const handleMobileMenuToggle = () => {
    setIsMobileMenuOpen(!isMobileMenuOpen);
    setMenuIcon(isMobileMenuOpen ? Menu : IconX);
  };

  const handleMobileMenuItemClick = (path) => {
    navigate(path);
    setIsMobileMenuOpen(false);
    setMenuIcon(Menu);
  };

  return (
    <>
      <div className="fixed left-0 right-0 top-0 z-[1000] bg-navy-5 font-montserrat transition-all duration-500 py-0 bg-transparent shadow-lg">
        <div className="container mx-auto flex h-14 items-center justify-between md:h-[70px] lg:justify-between sticky px-3 lg:px-0  ">
          <Link to="/" className="flex">
            <div
              className={
                isMobile
                  ? "relative h-[34px] w-[36px] md:h-[50px] md:w-[50px]"
                  : "relative  w-[82px]  md:w-[50px]"
              }
            >
              <img src={Logo} alt="Vercel Logo" layout="fill" priority />
            </div>
          </Link>
          <div className="">
            {" "}
            {isTablet && (
              <div className="flex justify-start items-center h-[40px] w-10">
                <button
                  className="lg:hidden mt-auto mb-auto focus:outline-none"
                  onClick={handleMobileMenuToggle}
                >
                  {/* <img src={Menu} alt="Menu" /> */}
                  <img
                    className=""
                    src={menuIcon}
                    alt="Picture of the author"
                    width={30}
                    height={30}
                  />
                </button>
              </div>
            )}
          </div>
          <ul
            className={`relative hidden items-start space-x-0 space-y-10 lg:flex lg:items-center lg:space-x-[2.25rem] lg:space-y-0 2xl:space-x-8 3xl:space-x-[60px]`}
          >
            {routes.map((x) => (
              <li
                onClick={() => {
                  navigate(x.path);
                  // setIsOpen(false);
                }}
                key={x.path}
                className={`text-white font-[500] group relative cursor-pointer font-[Noto Sans] text-lg leading-[24px] tracking-wider transition duration-300 lg:text-base lg:leading-5 xl:text-[14px] xl:leading-[14px]`}
              >
                {t(x.name)}
                <span className="block h-1 max-w-0 bg-[#FA4613] transition-all duration-700 group-hover:max-w-full"></span>
              </li>
            ))}
          </ul>

          <div className={`relative lg:flex hidden`}>
            <div className="pr-3">
              <div>
                <img src={Arrow} alt="verticalLine" />
              </div>
            </div>
          <div className="flex items-center">
          <div className=" lg:flex hidden text-white font-[500] group relative cursor-pointer font-[Noto Sans] text-lg leading-[24px] tracking-wider transition duration-300 lg:text-base lg:leading-5 xl:text-[14px] xl:leading-[14px]`}">
              <div>Connexion</div>
            </div>
          </div>
          </div>
        </div>

        {isTablet && isMobileMenuOpen && (
          <div className="lg:hidden">
            <ul className="bg-white py-2 pl-3">
              {routes.map((x) => (
                <li
                  key={x.path}
                  onClick={() => handleMobileMenuItemClick(x.path)}
                  className={`pb-6 font-bold group relative cursor-pointer font-sans text-lg leading-[24px] tracking-wider transition duration-300 lg:text-base lg:leading-5 xl:text-[20px] xl:leading-[22px] pt-3`}
                >
                  {t(x.name)}
                  <span className="block h-1 max-w-0 bg-[#FA4613] transition-all duration-700 group-hover:max-w-[15%]"></span>
                </li>
              ))}
            </ul>
          </div>
        )}
      </div>
    </>
  );
}
