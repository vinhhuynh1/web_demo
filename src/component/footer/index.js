import { useDevice } from "../../hook/useDevice";
import { Flex, Divider, img, Row, Col, Input, Button } from "antd";
import React from "react";
import { useTranslation } from "react-i18next";
import LogoIn from "../Image/Footer/linkedin.svg";
import LogoTW from "../Image/Footer/twitter.svg";
import LogoFB from "../Image/Footer/facebook.svg";
import LogoStagam from "../Image/Footer/instagram.svg";
import LogoMedium from "../Image/Footer/medium-m.svg";
import { Link } from "react-router-dom";
import Logo from "../Image/images/LogoOgma.svg";
const Footer = React.memo(() => {
  const { isMobile } = useDevice();
  const { t } = useTranslation(["translation"]);
  return (
    <footer className="bg-[#0B0B22]">
      <div className="container mx-auto">
        <div className={isMobile ? "container px-8" : "container"}>
          <div className="flex-col flex items-center pb-7">
            <div className="">
              <img
                src={Logo}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>
            <div>
              <div className="mt-[25px] flex">
                <Link>
                  <img
                    className="  mr-6"
                    src={LogoIn}
                    alt="Picture of the author"
                    width={30}
                    height={30}
                  />
                </Link>

                <Link>
                  <img
                    className=" mr-6"
                    src={LogoTW}
                    alt="Picture of the author"
                    width={30}
                    height={30}
                  />
                </Link>

                <Link>
                  <img
                    className=" mr-6"
                    src={LogoFB}
                    alt="Picture of the author"
                    width={30}
                    height={30}
                  />
                </Link>

                <Link>
                  <img
                    className="  mr-6"
                    src={LogoStagam}
                    alt="Picture of the author"
                    width={30}
                    height={30}
                  />
                </Link>
                <Link>
                  <img
                    className="  mr-6"
                    src={LogoMedium}
                    alt="Picture of the author"
                    width={30}
                    height={30}
                  />
                </Link>
              </div>
            </div>
          </div>
          <Row>
            <Col xs={24} xl={6}>
              <div className="mt-5">
                {/* logo */}
                <div className="flex items-center mb-5">
                  <Link href="/">
                    <div className={""}>
                      <p className="font-cabiner mt-3 text-2xl font-semibold leading-[22px] tracking-wider text-white">
                        About us
                      </p>
                    </div>
                  </Link>
                </div>
                {/* text */}
                <div>
                  <ul>
                    <li className="">
                      <div className="cusor-pointer mt-3 flex items-center text-[#9D9DA7] transition duration-300 hover:scale-110 hover:text-[#FA4613]">
                        <p className="font-sansFrancisco  text-[15px] font-normal leading-6 tracking-[0.03em]  ">
                          About Our Company
                        </p>
                      </div>
                    </li>
                    <li className="">
                      <div className="cusor-pointer mt-3 flex items-center text-[#9D9DA7] transition duration-300 hover:scale-110 hover:text-[#FA4613]">
                        <p className="font-sansFrancisco  text-[15px] font-normal leading-6 tracking-[0.03em]  ">
                          Careers
                        </p>
                      </div>
                    </li>
                  </ul>
                </div>

                <div className="text-center">
                  <Divider
                    type={isMobile ? "horizontal" : undefined}
                    className={isMobile ? "h-[1px] bg-black" : "hidden"}
                  />
                </div>
              </div>
            </Col>
            <Col xs={24} xl={6}>
              <div className={isMobile ? "relative" : "relative mt-8"}>
                <p className="font-cabiner mt-3 text-2xl font-semibold leading-[22px] tracking-wider text-white">
                  Our services
                </p>
                {/* text */}
                <div className="mt-5">
                  <ul>
                    <li className="">
                      <Link
                        href=""
                        className="cusor-pointer mt-3 flex items-center hover:scale-110"
                      >
                        <p className="font-sansFrancisco  text-[15px] font-normal leading-6 tracking-[0.03em] text-[#9D9DA7]  hover:text-[#FA4613] transition duration-300">
                          Lorem ispum abc random
                        </p>
                      </Link>
                    </li>
                    <li className="">
                      <Link
                        href=""
                        className="cusor-pointer mt-3 flex items-center  hover:scale-110"
                      >
                        <p className="font-sansFrancisco  text-[15px] font-normal leading-6 tracking-[0.03em]  text-[#9D9DA7] transition duration-300  hover:text-[#FA4613]">
                          Lorem ispum abc random
                        </p>
                      </Link>
                    </li>
                    <li className="">
                      <Link
                        href=""
                        className="cusor-pointer mt-3 flex items-center hover:scale-110 "
                      >
                        <p className="font-sansFrancisco  text-[15px] font-normal leading-6 tracking-[0.03em]  text-[#9D9DA7] transition duration-300 hover:text-[#FA4613] ">
                          Lorem ispum abc random
                        </p>
                      </Link>
                    </li>
                  </ul>
                </div>

                <div className="text-center">
                  <Divider
                    type={isMobile ? "horizontal" : undefined}
                    className={isMobile ? "h-[1px] bg-black" : "hidden"}
                  />
                </div>
              </div>
            </Col>
            <Col xs={24} xl={6}>
              <div className={isMobile ? "relative" : "relative ml-3 mt-8"}>
                <p className="font-cabiner mt-3 text-2xl font-semibold leading-[22px] tracking-wider text-white">
                  Functionalities
                </p>
                {/* text */}
                <div className="mt-5">
                  <ul>
                    <li className="cusor-pointer mt-3 flex items-center text-[#9D9DA7] transition duration-300 hover:scale-110 hover:text-[#FA4613] ">
                      <p className="font-sansFrancisco text-[15px] font-normal leading-6 tracking-[0.03em]">
                        Marketing
                      </p>
                    </li>
                    <li className="cusor-pointer mt-3 flex items-center text-[#9D9DA7] transition duration-300 hover:scale-110 hover:text-[#FA4613] ">
                      <p className="font-sansFrancisco text-[15px] font-normal leading-6 tracking-[0.03em]">
                        E-learning
                      </p>
                    </li>
                    <li className="cusor-pointer mt-3 flex items-center text-[#9D9DA7] transition duration-300 hover:scale-110 hover:text-[#FA4613] ">
                      <p className="font-sansFrancisco text-[15px] font-normal leading-6 tracking-[0.03em]">
                        CRM
                      </p>
                    </li>
                  </ul>
                </div>

                <div className="text-center">
                  <Divider
                    type={isMobile ? "horizontal" : undefined}
                    className={isMobile ? "h-[1px] bg-black" : "hidden"}
                  />
                </div>
              </div>
            </Col>
            <Col xs={24} xl={6}>
              <div className={isMobile ? "relative mb-8" : "relative my-8"}>
                <p className="font-cabiner mt-3 text-2xl font-semibold leading-[22px] tracking-wider text-white">
                  Contact Info
                </p>
                {/* video */}
                <div className="mt-5">
                  <ul>
                    <li className="cusor-pointer mt-3 flex items-center text-[#9D9DA7] transition duration-300 hover:scale-110 hover:text-[#FA4613] ">
                      <p className="font-sansFrancisco text-[15px] font-normal leading-6 tracking-[0.03em]">
                        +24 12 121251
                      </p>
                    </li>
                    <li className="cusor-pointer mt-3 flex items-center text-[#9D9DA7] transition duration-300 hover:scale-110 hover:text-[#FA4613] ">
                      <p className="font-sansFrancisco text-[15px] font-normal leading-6 tracking-[0.03em]">
                        1000 BX Amsterdam
                      </p>
                    </li>
                  </ul>
                </div>
                {/* icon */}
              </div>
            </Col>
          </Row>
        </div>

        <div className="h-[1px] bg-[#FFFFFF]"></div>
        <div className={isMobile ? "mb-5" : undefined}>
          <div className="flex justify-around">
            <p className=" mb-1 mt-1 text-center font-sansFrancisco text-[12px] font-normal italic leading-6 tracking-[0.03em] text-[#9D9DA7] pt-6 pb-6">
              Copyright © 2022 – OGMA Pte. Ltd.
            </p>
            <p className=" mb-1 mt-1 text-center font-sansFrancisco text-[12px] font-normal italic leading-6 tracking-[0.03em] text-[#9D9DA7] pt-6 pb-6">
              Mentions légales
            </p>
            <p className=" mb-1 mt-1 text-center font-sansFrancisco text-[12px] font-normal italic leading-6 tracking-[0.03em] text-[#9D9DA7] pt-6 pb-6">
              Conditions générales de ventes.
            </p>
          </div>
        </div>
      </div>
    </footer>
  );
});

Footer.displayName = "Footer";

export default Footer;
