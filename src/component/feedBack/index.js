import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { useDevice } from "../../hook/useDevice";

const dataFeedBack = [
  {
    name: "Vincent Hugo",
    avatar:
      "https://images.unsplash.com/photo-1633332755192-727a05c4013d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1480&q=80",
    job: "CEO Loom",
    describe:
    "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from de Finibus Bonorum et Malorum",
  },
  {
    name: "Victor Paul",
    avatar: "https://randomuser.me/api/portraits/men/1.jpg",
    job: "Head of Marketing Vulcan Labs",
    describe:
      "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from de Finibus Bonorum et Malorum",
  },
  {
    name: "Johny Dang",
    avatar: "https://randomuser.me/api/portraits/women/2.jpg",
    job: "Founder Bitlab",
    describe:
      "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from de Finibus Bonorum et Malorum",
  },
  {
    name: "Michael Smith",
    avatar: "https://randomuser.me/api/portraits/men/3.jpg",
    job: "Full Stack Developer",
    describe:
      "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from de Finibus Bonorum et Malorum",
  },
  {
    name: "Emily Johnson",
    avatar: "https://randomuser.me/api/portraits/women/4.jpg",
    job: "UI/UX Designer",
    describe:
      "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from de Finibus Bonorum et Malorum",
  },
];
const FeedBack = () => {
  const { isMobile } = useDevice();
  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: isMobile ? 1 : 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    speed: 500,
    arrows: false,
  };

  return (
    <div className="container mx-auto mb-5 ">
      <div className={isMobile ? "mx-[25px]" : undefined}>
        <Slider {...settings}>
          {dataFeedBack.map((data, index) => {
            return (
              <div key={index}>
                <div className="relative bg-[#232338] shadow-2xl border border-blue-500 flex w-full max-w-[26rem] flex-col rounded-xl bg-clip-border p-5">
                <div className="p-0 mb-3">
                    <p className="block font-sans text-[14px] antialiased font-light leading-relaxed text-white">
                      {data.describe}
                    </p>
                  </div>
                  <div className="relative flex items-center gap-4 pt-0 pb-8 mx-0 mt-2 overflow-hidden text-gray-700 bg-transparent shadow-none rounded-xl bg-clip-border ">
                    <img
                      src={data.avatar}
                      alt="Tania Andrew"
                      className="relative inline-block h-[58px] w-[58px] !rounded-full  object-cover object-center"
                    />
                    <div className="flex w-full flex-col gap-0.5">
                      <div className="flex items-center justify-between">
                        <h5 className="block font-sans text-base antialiased font-semibold leading-snug tracking-normal text-[#16FCD2]">
                          {data.name}
                        </h5>
                      </div>
                      <p className="block font-sans text-[14px] antialiased font-light leading-relaxed text-white">
                        {data.job}
                      </p>
                    </div>
                  </div>

                </div>
              </div>
            );
          })}
        </Slider>
      </div>
    </div>
  );
};

export default FeedBack;
