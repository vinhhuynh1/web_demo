

import * as React from "react";
import { useDevice } from "../../../hook/useDevice";
import Check from "../../Image/images/checkSuss.svg";
import funi from "../../Image/images/clickFuni.svg";
import LeadPage from "../../Image/images/leadPage.svg";
import Img_Plus1 from "../../Image/images/plus1.png";
import remplace from "../../Image/images/remplace.svg";
import System from "../../Image/images/system.svg";
export default function Plus1() {
  const { isMobile } = useDevice();

  return (
    <section className="container mx-auto lg:mt-[97px] xl:px-10 mt-[20px]">
      <div className="flex">
        <div className="">
          {" "}
          <img
            src={Img_Plus1}
            alt="Fail"
            layout="fill"
            className="object-cover object-left"
          />
        </div>
        <div className="pt-7">
          <div className="flex">
            <div className="text-white text-[14px] font-[700] font-[Noto Sans] leading-[48px] pr-6">
              <img
                src={remplace}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>
            <div className="text-white text-[14px] font-[700] font-[Noto Sans] leading-[48px] pr-6">
              <img
                src={funi}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>
            <div className="text-white text-[14px] font-[700] font-[Noto Sans] leading-[48px] pr-6">
              <img
                src={LeadPage}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>
            <div className="text-white text-[14px] font-[700] font-[Noto Sans] leading-[48px] pr-6">
              <img
                src={System}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>
          </div>
          <div className="text-white text-[30px] font-[700] font-[Noto Sans] leading-[48px]">
            Créez des tunnels de vente
          </div>
          <div className="text-white text-[30px] font-[700] font-[Noto Sans] leading-[48px]">
            et d’acquisition de leads
          </div>
          <div className="flex flex-col  pt-8">
            <div className="text-[#A79EA9] text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
              Vous avez beau avoir la meilleure formation mais il faut savoir la
            </div>
            <div className="text-[#A79EA9] text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
              montrer à la bonne cible et ensuite pouvoir transformer votre
            </div>
            <div className="text-[#A79EA9] text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
              ead en clients en automatique. Si aujourd’hui vous n’avez de
            </div>
            <div className="text-[#A79EA9] text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
              tunnel de vente vous laissez au concurrent vos apprenants.
            </div>
          </div>
          <div className="pt-14 pb-14">
            <button className="bg-[#DD5313] text-white pr-12 pl-12 pt-4 pb-4 text-[16px] rounded leading-[22px] font-[600] hover:bg-white hover:text-[#197C6A]">
              Cliquer ici pour créer un compte gratuit
            </button>
          </div>
          <div className="flex items-center">
            <div className=" pr-3">
              <img
                src={Check}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>
            <div className="text-[#A79EA9] text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
            100% Gratuit
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
