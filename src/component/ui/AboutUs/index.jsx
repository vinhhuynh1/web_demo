import { useAnimation } from "framer-motion";

import * as React from "react";
import { useInView } from "react-intersection-observer";
import { useDevice } from "../../../hook/useDevice";
import Plus1 from "../../../component/ui/Plus1/index";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Plus2 from "../../../component/ui/Plus2/index";
import Plus3 from "../Plus3";
import Plus5 from "../Plus5";
import Plus4 from "../Plus4";
export default function AboutUs() {
  const { isMobile } = useDevice();

  return (
    <section className="container mx-auto lg:mt-[97px] xl:px-10 mt-[20px] flex justify-center">
      <div className="pb-5"> </div>
      <div className="">
        <div className="flex flex-col items-center">
          <div>
            <div className="flex justify-center  text-white text-[30px] font-[700] font-[Noto Sans] leading-[48px]">
              Plus de 9 outils inclus
            </div>
          </div>
          <Plus1 />
          <Plus2 />
          <Plus3 />
          <Plus4 />
          <Plus5 />
        </div>
      </div>
    </section>
  );
}
