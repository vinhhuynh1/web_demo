import React from "react";
import Securyty from "../../Image/images/security.svg";
import YTB from "../../Image/images/ytb_ios.png";
import FeedBack from "../../../component/feedBack";
import { Card } from "antd";
import styles from "./index.module.css";
const Advantages = () => {
  const dataKey = [
    {
      img: { Securyty },
      name: "Avantage 1",
      title1:
        "Sécurité et protection de vos données Il est important pour nous que vos données ainsi que celle de vos apprenants soient protégées. Nous utilisons un logiciel bien spécifique à haute protection.",
    },
    {
      img: { Securyty },
      name: "Avantage 2",
      title1:
        "Sécurité et protection de vos données Il est important pour nous que vos données ainsi que celle de vos apprenants soient protégées. Nous utilisons un logiciel bien spécifique à haute protection.",
    },
    {
      img: { Securyty },
      name: "Avantage 3",
      title1:
        "Sécurité et protection de vos données Il est important pour nous que vos données ainsi que celle de vos apprenants soient protégées. Nous utilisons un logiciel bien spécifique à haute protection.",
    },
    {
      img: { Securyty },
      name: "Avantage 4",
      title1:
        "Sécurité et protection de vos données Il est important pour nous que vos données ainsi que celle de vos apprenants soient protégées. Nous utilisons un logiciel bien spécifique à haute protection.",
    },
    {
      img: { Securyty },
      name: "Avantage 5",
      title1:
        "Sécurité et protection de vos données Il est important pour nous que vos données ainsi que celle de vos apprenants soient protégées. Nous utilisons un logiciel bien spécifique à haute protection.",
    },
    {
      img: { Securyty },
      name: "Avantage 6",
      title1:
        "Sécurité et protection de vos données Il est important pour nous que vos données ainsi que celle de vos apprenants soient protégées. Nous utilisons un logiciel bien spécifique à haute protection.",
    },
  ];

  return (
    <div className="relative pt-20 pb-20 lg:px-72 px-0">
      <div className="flex flex-col items-center pt-16">
        <div className="text-[#16FCD2] text-[16px] font-[700] font-[Noto Sans] leading-[48px]">
          ABOUT METAFORMA
        </div>
        <div className="flex flex-col items-center pb-14">
          <div className="text-white text-[30px] font-[700] font-[Noto Sans] leading-[48px]">
            Here's what you will get when
          </div>
          <div className="text-white text-[30px] font-[700] font-[Noto Sans] leading-[48px]">
            purchasing this service
          </div>
        </div>
      </div>
      <div className="grid grid-cols-3 gap-4">
        {dataKey.map((data, index) => {
          return (
            <div key={index}>
              <div className="">
                <Card className={`${styles.bg_transparent}`}>
                  <div className="pl-1 pr-10">
                    <div className="flex pt-0 pb-6">
                      <div className="pr-5">
                        <img
                          src={Securyty}
                          alt="Fail"
                          layout="fill"
                          className="object-cover object-left"
                        />
                      </div>
                      <div className="text-white text-[16px] font-[600] font-[Noto Sans] leading-[48px]">
                       {data.name}
                      </div>
                    </div>
                    <div className="text-[#91919B] text-[14px] font-[400] font-[Noto Sans] leading-[24px]">
                      {data.title1}
                    </div>
                  </div>
                </Card>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default React.memo(Advantages);
