import * as React from "react";
import { useDevice } from "../../../hook/useDevice";
import Check from "../../Image/images/checkSuss.svg";
import funi from "../../Image/images/clickFuni.svg";
import LeadPage from "../../Image/images/leadPage.svg";
import Img_Plus2 from "../../Image/images/plus2.png";
import remplace from "../../Image/images/remplace.svg";
import Digi from "../../Image/images/digi.svg";
import Podia from "../../Image/images/podia.svg";
import Teach from "../../Image/images/teach.svg";
import Leany from "../../Image/images/learyBox.svg";
import Plus4_img from "../../Image/images/img_4.png";
import System from "../../Image/images/system.svg";
export default function Plus4() {
  const { isMobile } = useDevice();

  return (
    <section className="container mx-auto lg:mt-[97px] xl:px-10 mt-[20px]">
      <div className="flex">
        <div className="pt-7">
          <div className="flex">
            <div className="text-white text-[14px] font-[700] font-[Noto Sans] leading-[48px] pr-6">
              <img
                src={remplace}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>
            <div className="text-white text-[14px] font-[700] font-[Noto Sans] leading-[48px] pr-6">
              <img
                src={Digi}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>
            <div className="text-white text-[14px] font-[700] font-[Noto Sans] leading-[48px] pr-6">
              <img
                src={Podia}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>
            <div className="text-white text-[14px] font-[700] font-[Noto Sans] leading-[48px] pr-6">
              <img
                src={Teach}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>
            <div className="text-white text-[14px] font-[700] font-[Noto Sans] leading-[48px] pr-6">
              <img
                src={Leany}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>
          </div>
          <div className="text-white text-[30px] font-[700] font-[Noto Sans] leading-[48px]">
            Créer des formation
          </div>
          <div className="text-white text-[30px] font-[700] font-[Noto Sans] leading-[48px]">
            en ligne
          </div>
          <div className="flex flex-col  pt-8">
            <div className="text-white text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
              Un espace dédié pour vos formations et espace membre créé
            </div>
            <div className="text-white text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
              avec intelligence et simplicité pour vous comme pour vos
            </div>
            <div className="text-white text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
              apprenants. Un espace de discussion (forum) pour chacune de
            </div>
            <div className="text-white text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
              vos formations pour un échange fluide et organisé. Vous
            </div>
            <div className="text-white text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
              apprenant vont adorer.
            </div>
          </div>
          <div className="pt-14 pb-14">
            <button className="bg-[#DD5313] text-white pr-12 pl-12 pt-4 pb-4 text-[16px] rounded leading-[22px] font-[600] hover:bg-white hover:text-[#197C6A]">
              Cliquer ici pour créer un compte gratuit
            </button>
          </div>
          <div className="flex items-center">
            <div className=" pr-3">
              <img
                src={Check}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>
            <div className="text-[#A79EA9] text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
              100% Gratuit
            </div>
          </div>
        </div>
        <div className="">
          {" "}
          <img
            src={Plus4_img}
            alt="Fail"
            layout="fill"
            className="object-cover object-left"
          />
        </div>
      </div>
    </section>
  );
}
