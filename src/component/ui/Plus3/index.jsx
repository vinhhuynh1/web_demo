import * as React from "react";
import { useDevice } from "../../../hook/useDevice";
import Check from "../../Image/images/checkSuss.svg";
import funi from "../../Image/images/clickFuni.svg";
import LeadPage from "../../Image/images/leadPage.svg";
import Img_Plus3 from "../../Image/images/img_plus3.png";
import remplace from "../../Image/images/remplace.svg";
import System from "../../Image/images/system.svg";
export default function Plus3() {
  const { isMobile } = useDevice();

  return (
    <section className="container mx-auto lg:mt-[97px] xl:px-10 mt-[20px]">
      <div className="flex justify-center">
        <div className="pt-7">
          <div className="flex justify-center">
            <div className="text-white text-[14px] font-[700] font-[Noto Sans] leading-[48px] pr-6">
              <img
                src={remplace}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>
            <div className="text-white text-[14px] font-[700] font-[Noto Sans] leading-[48px] pr-6">
              <img
                src={funi}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>
            <div className="text-white text-[14px] font-[700] font-[Noto Sans] leading-[48px] pr-6">
              <img
                src={LeadPage}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>
            <div className="text-white text-[14px] font-[700] font-[Noto Sans] leading-[48px] pr-6">
              <img
                src={System}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>
          </div>
          <div className="text-white text-[30px] font-[700] font-[Noto Sans] leading-[48px] flex justify-center">
            Créez des sites internet
          </div>
          <div className="flex flex-col  pt-8 items-center">
            <div className="text-[#A79EA9] text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
              En plus des tunnels de vente il vous faut un site internet
              professionnel car votre organisme de
            </div>
            <div className="text-[#A79EA9] text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
              formation le mérite. Nous avons conçu Meta Forma pour que vous
              puissiez créer votre site
            </div>
            <div className="text-[#A79EA9] text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
              éinternet très rapidement, facilement sans aucun codage et surtout
              moderne. Une chose est
            </div>
            <div className="text-[#A79EA9] text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
              sûre, la concurrence sera jalouse de votre beau site internet.
            </div>
          </div>
          <div className="pt-10 pb-2 flex justify-center">
            <button className="bg-[#DD5313] text-white pr-12 pl-12 pt-4 pb-4 text-[16px] rounded leading-[22px] font-[600] hover:bg-white hover:text-[#197C6A]">
              Cliquer ici pour créer un compte gratuit
            </button>
          </div>
          <div className="flex items-center justify-center">
            <div className=" pr-3">
              <img
                src={Check}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>
            <div className="text-[#A79EA9] text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
              100% Gratuit
            </div>
          </div>
        </div>
      </div>
      <div className="flex justify-center">
        <img
          src={Img_Plus3}
          alt="Fail"
          layout="fill"
          className="object-cover object-left"
        />
      </div>
    </section>
  );
}
