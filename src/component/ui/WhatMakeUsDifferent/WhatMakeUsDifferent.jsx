import Chart from "../../Image/images/chart.png";
import Check from "../../Image/images/checkSuss.svg";
import Email from "../../Image/images/submit_email.png";
function WhatMakeUsDifferent() {
  return (
    <section className="px-4 md:px-0 xl:px-10 2xl:px-[5%]">
      <div className="flex justify-center">
        <div className="">
          {" "}
          <img
            src={Chart}
            alt="Fail"
            layout="fill"
            className="object-cover object-left"
          />
        </div>
        <div className="">
          <div className="">
            <div className="text-[#16FCD2] text-[16px] font-[600] font-[Noto Sans] leading-[32px]">
              PRICING
            </div>
            <div className="text-white text-[30px] font-[700] font-[Noto Sans] leading-[48px] pb-5">
              Valeur du logiciel
            </div>
          </div>
          <div className="pb-10">
            <div className="text-[#9D9DA7] text-[16px] font-[600] font-[Noto Sans] leading-[32px]">
              Pour avoir un organisme de formation qui dépasse minimum les
            </div>
            <div className="text-white text-[16px] font-[600] font-[Noto Sans] leading-[32px]">
              6 chiffres par an de Chiffre d’affaire il vous faut :
            </div>
          </div>
          {/* --------------- */}
          <div className="pl-5">
            <div className="flex pb-3">
              <div className="pr-4">
                <img
                  src={Check}
                  alt="Fail"
                  layout="fill"
                  className="object-cover object-left"
                />
              </div>
              <div className="text-white text-[16px] font-[600] font-[Noto Sans] leading-[32px">
                Tunnel de vente Pro: commence à 97€/mois type activecampaign{" "}
              </div>
            </div>
            <div className="flex pb-3">
              <div className="pr-4">
                <img
                  src={Check}
                  alt="Fail"
                  layout="fill"
                  className="object-cover object-left"
                />
              </div>
              <div className="text-white text-[16px] font-[600] font-[Noto Sans] leading-[32px">
                Email marketing Pro: commence à 29€/mois (type activecampaign)
              </div>
            </div>
            <div className="flex pb-3">
              <div className="pr-4">
                <img
                  src={Check}
                  alt="Fail"
                  layout="fill"
                  className="object-cover object-left"
                />
              </div>
              <div className="text-white text-[16px] font-[600] font-[Noto Sans] leading-[32px">
                Espace membres Pro: commence à 119€/mois (type kajabi)
              </div>
            </div>
            <div className="flex pb-3">
              <div className="pr-4">
                <img
                  src={Check}
                  alt="Fail"
                  layout="fill"
                  className="object-cover object-left"
                />
              </div>
              <div className="text-white text-[16px] font-[600] font-[Noto Sans] leading-[32px">
                CRM Pro: commence à 41€/mois (type hubspot)
              </div>
            </div>
            <div className="flex pb-3">
              <div className="pr-4">
                <img
                  src={Check}
                  alt="Fail"
                  layout="fill"
                  className="object-cover object-left"
                />
              </div>
              <div className="text-white text-[16px] font-[600] font-[Noto Sans] leading-[32px">
                Administratif qualiopi : commence à 99€/mois (type digiforma)
              </div>
            </div>
            <div className="flex pb-3">
              <div className="pr-4">
                <img
                  src={Check}
                  alt="Fail"
                  layout="fill"
                  className="object-cover object-left"
                />
              </div>
              <div className="text-white text-[16px] font-[600] font-[Noto Sans] leading-[32px">
                Signature électronique: commence à 23€/mois (type docusign)
              </div>
            </div>
            <div className="flex pb-3">
              <div className="pr-4">
                <img
                  src={Check}
                  alt="Fail"
                  layout="fill"
                  className="object-cover object-left"
                />
              </div>
              <div className="text-white text-[16px] font-[600] font-[Noto Sans] leading-[32px">
                Comptabilité: commence à 99€/mois (type ipaidthat)
              </div>
            </div>
            <div className="flex pb-3">
              <div className="pr-4">
                <img
                  src={Check}
                  alt="Fail"
                  layout="fill"
                  className="object-cover object-left"
                />
              </div>
              <div className="text-white text-[16px] font-[600] font-[Noto Sans] leading-[32px">
                Webinare: Commence à 99€/mois (type livestorm)
              </div>
            </div>
          </div>
          <div className="text-[#9D9DA7] text-[16px] font-[600] font-[Noto Sans] leading-[32px]">
            Et bien plus encore… Car Meta Forma garde encore ses petits
          </div>
          <div className="text-[#9D9DA7] text-[16px] font-[600] font-[Noto Sans] leading-[32px]">
            secrets pour vous surprendre encore plus !
          </div>
        </div>
      </div>
      <div className="flex justify-center pt-40 pb-20">
        <img
          src={Email}
          alt="Fail"
          layout="fill"
          className="object-cover object-left"
        />
      </div>
    </section>
  );
}

export default WhatMakeUsDifferent;
