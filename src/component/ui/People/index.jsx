import React from "react";
import Peoples from "../../Image/images/people.png";
import styles from "./index.module.css";
const People = () => {
  const dataKey = [
    {
      img: { Peoples },
      name: "Organisme de formation",
    },
    {
      img: { Peoples },
      name: "Infopreneur",
    },
    {
      img: { Peoples },
      name: "Coach",
    },
    {
      img: { Peoples },
      name: "Consultant",
    },
    {
      img: { Peoples },
      name: "Formateur",
    },
    {
      img: { Peoples },
      name: "Professeur",
    },
    {
      img: { Peoples },
      name: "Thérapeute",
    },
    {
      img: { Peoples },
      name: "Entrepreneur",
    },
  ];

  return (
    <div className="relative pt-20 pb-20 lg:px-72 px-0">
      <div className="flex flex-col items-center pt-16">
        <div className="flex flex-col items-center pb-14">
          <div className="text-white text-[30px] font-[700] font-[Noto Sans] leading-[48px]">
            Un outil professionnel tout en un pour:
          </div>
        </div>
      </div>
      <div className="grid grid-cols-4 gap-4">
        {dataKey.map((data, index) => {
          return (
            <div key={index}>
              <div className="">
                <div className={`${styles.bg_transparent}`}>
                  <div className="pl-1 pr-10 flex items-center flex-col pb-10">
                    <div className="flex pt-0 pb-6">
                      <div className="text-[#16FCD2] text-[16px] font-[700] font-[Noto Sans] leading-[48px]">
                        {data.name}
                      </div>
                    </div>
                    <div className="pr-5">
                      <img
                        src={Peoples}
                        alt="Fail"
                        layout="fill"
                        className="object-cover object-left"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default React.memo(People);
