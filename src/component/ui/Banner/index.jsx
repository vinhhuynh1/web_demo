import React from "react";
import Input from "../../Image/images/input.png";
import YTB from "../../Image/images/ytb_ios.png";
import FeedBack from "../../../component/feedBack";
const Banner = () => {
  return (
    <div className="relative pt-20">
      <div className="flex flex-col items-center pt-16">
        <div className="text-[#16FCD2] text-[16px] font-[700] font-[Noto Sans] leading-[48px]">
          ABOUT METAFORMA
        </div>
        <div className="flex flex-col items-center">
          <div className="text-white text-[30px] font-[700] font-[Noto Sans] leading-[48px]">
            La plateforme tout en un pour les professionnels de la
          </div>
          <div className="text-white text-[30px] font-[700] font-[Noto Sans] leading-[48px]">
            formation la plus simple et automatisée au monde
          </div>
        </div>
        <div className="flex flex-col items-center pt-8">
          <div className="text-[#A79EA9] text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
            MetaForma helps you make more sales by helping your
          </div>
          <div className="text-[#A79EA9] text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
            customers get more results
          </div>
        </div>
        <div className="pt-7">
          {/* <img
            src={Input}
            alt="Fail"
            layout="fill"
            className="object-cover object-left"
          /> */}
          <div className="flex">
            <div className="pr-5">
              <input
                type="text"
                name="name"
                placeholder="Votre Adresse email Pro"
                className="w-[331px] h-[55px] rounded-l leading-[48px] px-4"
              />
            </div>
            <button className="bg-[#DD5313] text-white pr-7 pl-7 pt-4 pb-4 text-[16px] rounded leading-[22px] font-[600] hover:bg-white hover:text-[#197C6A]">
              Creer mon compte
            </button>
          </div>
        </div>
        <div className="pt-7">
          <img
            src={YTB}
            alt="Fail"
            layout="fill"
            className="object-cover object-left"
          />
        </div>
        <FeedBack />
      </div>
    </div>
  );
};

export default React.memo(Banner);
