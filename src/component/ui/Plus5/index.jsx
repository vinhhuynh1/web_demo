import * as React from "react";
import { useDevice } from "../../../hook/useDevice";
import Check from "../../Image/images/checkSuss.svg";
import funi from "../../Image/images/clickFuni.svg";
import LeadPage from "../../Image/images/leadPage.svg";
import Img_Plus4 from "../../Image/images/img_flus4.png";
import remplace from "../../Image/images/remplace.svg";
import learyBox from "../../Image/images/learyBox.svg";
import katrat from "../../Image/images/katrat.svg";
import System from "../../Image/images/system.svg";
export default function Plus5() {
  const { isMobile } = useDevice();

  return (
    <section className="container mx-auto lg:mt-[97px] xl:px-10 mt-[20px]">
      <div className="flex justify-center">
        <div className="">
          {" "}
          <img
            src={Img_Plus4}
            alt="Fail"
            layout="fill"
            className="object-cover object-left"
          />
        </div>
        <div className="pt-7">
          <div className="flex">
            <div className="text-white text-[14px] font-[700] font-[Noto Sans] leading-[48px] pr-6">
              <img
                src={remplace}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>
            <div className="text-white text-[14px] font-[700] font-[Noto Sans] leading-[48px] pr-6">
              <img
                src={funi}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>

            <div className="text-white text-[14px] font-[700] font-[Noto Sans] leading-[48px] pr-6">
              <img
                src={System}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>
            <div className="text-white text-[14px] font-[700] font-[Noto Sans] leading-[48px] pr-6">
              <img
                src={learyBox}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>
            <div className="text-white text-[14px] font-[700] font-[Noto Sans] leading-[48px] pr-6">
              <img
                src={katrat}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>
          </div>
          <div className="text-white text-[30px] font-[700] font-[Noto Sans] leading-[48px]">
            Automatisez vos taches à 90%
          </div>
          <div className="flex flex-col  pt-8">
            <div className="text-white text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
              Le plus important dans votre OF c’est vous et votre expertise.
            </div>
            <div className="text-white text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
              Avez-vous créé un organisme de formation pour faire de la
            </div>
            <div className="text-white text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
              paperasse, du marketing et de tout le temps vous occuper de
            </div>
            <div className="text-white text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
              trouver de nouveaux apprenants? La réponse et non votre zone
            </div>
            <div className="text-white text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
              de génie c’est de former et d’apporter de nouvelles compétences
            </div>
            <div className="text-white text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
              à vos apprenants. L’automatisation, vous allez …
            </div>
          </div>
          <div className="pt-14 pb-14">
            <button className="bg-[#DD5313] text-white pr-12 pl-12 pt-4 pb-4 text-[16px] rounded leading-[22px] font-[600] hover:bg-white hover:text-[#197C6A]">
              Cliquer ici pour créer un compte gratuit
            </button>
          </div>
          <div className="flex items-center">
            <div className=" pr-3">
              <img
                src={Check}
                alt="Fail"
                layout="fill"
                className="object-cover object-left"
              />
            </div>
            <div className="text-[#A79EA9] text-[16px] font-[500] font-[Noto Sans] leading-[32px]">
              100% Gratuit
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
